pub mod components;
pub mod systems;
pub mod gamestates;
pub mod bundles;

use bevy::prelude::*;
use self::systems::setup::*;
use self::gamestates::AppState;

fn main() {
    App::build()
        .add_plugins(DefaultPlugins)
        .add_state(AppState::Menu)
        .add_startup_system(setup_camera.system())
        .add_startup_system(setup_board.system())
        .run();
}