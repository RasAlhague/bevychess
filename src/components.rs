// Chesspiece related components
pub enum Piece {
    Black,
    White,
}
pub struct PieceValue(u8);
pub struct King;
pub struct Queen;
pub struct Bishop;
pub struct Rook;
pub struct Knight;
pub struct Pawn;
pub struct Moveset(Vec<(u32, u32)>);

// Player related components
pub struct Player;
pub struct Human;
pub struct AI;

// Board related
pub struct Board;