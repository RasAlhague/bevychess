pub mod setup {
    use bevy::prelude::*;

    pub fn setup_camera(mut commands: Commands) {
        commands.spawn_bundle(OrthographicCameraBundle::new_2d());
    }

    pub fn setup_board(mut commands: Commands, asset_server: Res<AssetServer>, mut materials: ResMut<Assets<ColorMaterial>>) {
        let texture_handle = asset_server.load("textures/board/chessboard.png");
        let transform = Transform::from_xyz(0.0, 0.0, 0.0);

        commands.spawn_bundle(SpriteBundle {
            material: materials.add(texture_handle.into()),
            transform: transform,
            ..Default::default()
        });
    }

    pub fn setup_players() {
        
    }
    
    pub fn setup_pieces() {
        
    }
}